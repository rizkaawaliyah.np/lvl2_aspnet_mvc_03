﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using System.Web.Util;
using LVL2_ASPNet_MVC_03.Models;

namespace LVL2_ASPNet_MVC_03.Controllers
{
    public class CustomerController : Controller
    {
        Db_CustomerEntities dbModel = new Db_CustomerEntities();

        // GET: Customer
        public ActionResult Index()
        {
            return View(dbModel.Tbl_Customer.ToList());
        }

        // GET: Customer/Details/5
        public ActionResult Details(int id)
        {           
            return View(dbModel.Tbl_Customer.Where(x => x.id == id).FirstOrDefault());
        }

        // GET: Customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Customer/Create
        [HttpPost]
        public ActionResult Create(Tbl_Customer customer)
        {
            using (var transactions = dbModel.Database.BeginTransaction())
            {
                try
                {
                    // TODO: Add insert logic here
                    dbModel.Tbl_Customer.Add(customer);
                    dbModel.SaveChanges();

                    Tbl_Customer Edit = dbModel.Tbl_Customer.Where(b => b.id == 4).FirstOrDefault();
                    Edit.Description = "Jakarta Barat";
                    dbModel.Entry(Edit).State = EntityState.Modified;
                    dbModel.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("Index");
                }
                /*
                catch(DbEntityValidationException err)
                {
                    ViewBag.err = "Invalid length";
                    return View();
                }*/
                catch (Exception err)
                {
                    var ErrorPosition = "Create_Customer";
                    Db_CustomerEntities dbError = new Db_CustomerEntities();
                    Tbl_Log_History error = new Tbl_Log_History {
                        ErrorMsg = err.Message,
                        ErrorLocation = ErrorPosition,
                        ErrorDate = DateTime.Now
                    };
                    dbError.Tbl_Log_History.Add(error);
                    dbError.SaveChanges();
                    transactions.Rollback();
                    ViewBag.err = err.Message;
                    return View();
                }
            }
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View(dbModel.Tbl_Customer.Where(x => x.id == id).FirstOrDefault());
        }

        // POST: Customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Tbl_Customer customer)
        {
            try
            {
                // TODO: Add update logic here
                dbModel.Entry(customer).State = EntityState.Modified;
                dbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View(dbModel.Tbl_Customer.Where(x => x.id == id).FirstOrDefault());
        }

        // POST: Customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Tbl_Customer customer)
        {
            try
            {
                // TODO: Add delete logic here
                customer = dbModel.Tbl_Customer.Where(x => x.id == id).FirstOrDefault();
                dbModel.Tbl_Customer.Remove(customer);
                dbModel.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
